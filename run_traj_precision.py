#################################################################################
#
# Trajectory test
#
#################################################################################

import util_traj
import trajectory
import os
import numpy as np
import copy
import matplotlib.pyplot as plt
import math
import sys
import cv2
import time

# Root directory of the project
ROOT_DIR = os.getcwd()

sys.path.append(os.path.join(ROOT_DIR,'../tracker/'))
import cameramodel as cm

# TRAJ_PATH = '/home/caor/Documents/code/datasets/carla_exp_6/camera_1/output/smoothed_no_3Dbox/csv';
# TRAJ_PATH_TRUTH = '/home/caor/Documents/code/datasets/carla_exp_6/2018-11-14_16:41:20';

# TRAJ_PATH = '/home/caor/Documents/code/datasets/carla_exp_6/camera_2/output/smoothed_no_3Dbox/csv';
# TRAJ_PATH = '/home/caor/Documents/code/datasets/carla_exp_6/camera_2/output/smoothed_mid_bottom_edge/csv';
# TRAJ_PATH = '/home/caor/Documents/code/datasets/carla_exp_6/camera_2/output/smoothed/csv';
# TRAJ_PATH_TRUTH = '/home/caor/Documents/code/datasets/carla_exp_6/2018-11-14_16:41:20';

# TRAJ_PATH = '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera2/output/smoothed_CV/csv';
# TRAJ_PATH_TRUTH = '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00';


# TRAJ_PATH = '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera2/output/smoothed_CV/csv';
# TRAJ_PATH_TRUTH = '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09';

TRAJ_PATH = '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/SceneCaptures_camera2/output/smoothed_CV/csv';
TRAJ_PATH_TRUTH = '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/2019-01-29_13:46:15';




def plot_trajectrories_on_images(traj_list, time_ms, img_street, img_sat, cam_model_street, cam_model_sat, color_text = (0,0,255)):

    for traj in traj_list:

        # =========================================
        #               Plot filtering
        # ==========================================


        traj_point = traj.get_point_at_timestamp(time_ms);
        if not (traj_point is None):

            if not (img_sat is None):

                # print('time_ms: {} Traj id: {} x: {} y: {}'.format(time_ms, traj.get_id(), traj_point.x, traj_point.y));

                # Reproject on the satellite image
                pt_pix = cam_model_sat.project_points(np.array([(traj_point.x, traj_point.y, 0.0)]));
                pt_pix = (int(pt_pix[0]),int(pt_pix[1]));
                img_sat = cv2.circle(img_sat, pt_pix,3, traj.get_color(), -1);

                # Add annotations to the track:
                text = "id: %i" % (traj.get_id());
                img_sat = cv2.putText(img_sat, text, pt_pix, cv2.FONT_HERSHEY_COMPLEX, 0.8, color_text, 1)

            # if not (img_street is None):
            #     # Reproject on the satellite image
            #     pt_pix = cam_model_street.project_points(np.array([(traj_point.x, traj_point.y, 0.0)]));
            #     pt_pix = (int(pt_pix[0]),int(pt_pix[1]));
            #     cv2.circle(img_street, pt_pix,3, traj.get_color(), -1);

            #     # Add annotations to the track:
            #     text = "id: %i" % (traj.get_id());
            #     img_street = cv2.putText(img_street, text, pt_pix, cv2.FONT_HERSHEY_COMPLEX, 0.8, (0, 0, 255), 1)

    return img_sat, img_street

# https://thispointer.com/python-how-to-remove-duplicates-from-a-list/
def removeDuplicates(listofElements):

    # Create an empty list to store unique elements
    uniqueList = []

    # Iterate over the original list and for each element
    # add it to uniqueList, if its not already there.
    for elem in listofElements:
        if elem not in uniqueList:
            uniqueList.append(elem)

    # Return the list of unique elements
    return uniqueList


def compute_precision(traj_list, traj_list_truth, display_hist=False, ):

    print('[INFO]: Trajectories {}'.format(len(traj_list)))

    for traj in traj_list_truth:
        print('[INFO]: Traj {} lenght: {} start_t: {} end_t: {}'.format(traj.get_id(), traj.get_length_ms(), traj.get_traj()[0].time_ms, traj.get_traj()[-1].time_ms));


    total_legnth_ms = 0;
    for traj in list(traj_list):
        if traj.get_length_ms() < 1000:
            print('Removing traj: {} traj length: {} ms'.format(traj.get_id(), traj.get_length_ms()));
            traj_list.remove(traj);
        else:
            total_legnth_ms = total_legnth_ms + traj.get_length_ms();

    result_error_xy = [];
    result_error_vxvy = [];
    result_error_psi_rad = [];


    result_traj_truth_list = [];
    result_traj_list = [];

    for traj_1 in traj_list:

        # Test to find the closest match:
        index, traj_min = util_traj.find_closest_traj(traj_1, traj_list_truth, traj_included = True);
        result_traj_truth_list.append(traj_min);
        result_traj_list.append(traj_1);

        if not (traj_min is None):
            error_xy, error_vxvy, error_psi_rad = traj_1.compute_error_to_traj(traj_min);

            print('Traj id: {} match traj truth id: {}'.format(traj_1.get_id(), traj_min.get_id()))
            print('Closest traj index: {}'.format(index));
            print('Result: error mean: xy:{} vxvy:{} psi_rad:{}'.format(np.mean(error_xy), np.mean(error_vxvy), np.mean(error_psi_rad)))
            print('')
            result_error_xy.append(np.mean(error_xy));
            result_error_vxvy.append(np.mean(error_vxvy));
            result_error_psi_rad.append(np.mean(error_psi_rad));


        # if math.isnan((np.mean(error))):
        #     print('Result: error: {}'.format((error)))


    for e in list(result_error_xy):
        if e > 10:
            result_error_xy.remove(e);

    result_error_xy = np.array(result_error_xy);


    error_xy = np.nanmean(result_error_xy);
    error_vxvy = np.nanmean(result_error_vxvy);
    error_psi_rad =  np.nanmean(result_error_psi_rad);
    # result_error = np.nan_to_num(result_error);
    print(result_error_xy);
    print('**************************')
    print('Average Error: xy:{} vxvy:{} psi_rad:{}'.format(error_xy, error_vxvy, error_psi_rad));
    print('Trajectories {}'.format(len(traj_list)))
    print('Total trajectories time: {}s'.format(float(total_legnth_ms)/float(1e3)))
    print('**************************')

    if display_hist:

        plt.figure('Hist xy')
        plt.hist(result_error_xy, bins=100);

        plt.figure('Hist vxvy')
        plt.hist(result_error_vxvy, bins=100);

        plt.figure('Hist psi (rad)')
        plt.hist(result_error_psi_rad, bins=100);

        plt.show()


    return error_xy, error_vxvy, error_psi_rad, result_traj_list, result_traj_truth_list;


def show_sat_img(cam_model_sat, img_sat, traj_list, traj_truth_list):

    # time_ms_list = [];
    # for traj in traj_list:
    #     time_ms_list = time_ms_list +traj.get_time_ms_list();
    # time_ms_list = removeDuplicates(time_ms_list);


    # for time_ms in time_ms_list:

    #     img_sat_temp = copy.copy(img_sat);
    #     img_sat_temp, img_street = plot_trajectrories_on_images(traj_list, time_ms, None, img_sat_temp, None, cam_model_sat);

    #     cv2.imshow('img_sat', img_sat_temp)

    #     # Normal Mode: Press q to exit
    #     if cv2.waitKey(1) & 0xFF == ord('q'):
    #         break;
    #     print('time_ms: {}'.format(time_ms))


    for traj, traj_truth in zip(traj_list, traj_truth_list):

        time_ms_list = traj.get_time_ms_list();

        for time_ms in time_ms_list:

            img_sat_temp = copy.copy(img_sat);
            img_sat_temp, img_street = plot_trajectrories_on_images([traj], time_ms, None, img_sat_temp, None, cam_model_sat, color_text=(0,0,255));
            img_sat_temp, img_street = plot_trajectrories_on_images([traj_truth], time_ms, None, img_sat_temp, None, cam_model_sat, color_text=(255,0,0));

            cv2.imshow('img_sat', img_sat_temp)

            # Normal Mode: Press q to exit
            if cv2.waitKey(1) & 0xFF == ord('q'):
                return;

            print('time_ms: {}'.format(time_ms))
            time.sleep(0.067);


def main():


    # traj_path_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera2/output/smoothed_CV_box3d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera3/output/smoothed_CV_box3d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera2/output/smoothed_CV_box3d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera3/output/smoothed_CV_box3d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/SceneCaptures_camera2/output/smoothed_CV_box3d/csv'];


    # traj_path_truth_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/2019-01-29_13:46:15'];



    # traj_path_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera2/output/smoothed_CV_center_2d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera3/output/smoothed_CV_center_2d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera2/output/smoothed_CV_center_2d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera3/output/smoothed_CV_center_2d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/SceneCaptures_camera2/output/smoothed_CV_center_2d/csv'];


    # traj_path_truth_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/2019-01-29_13:46:15'];

    # traj_path_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera2/output/smoothed_CV_center_bottom_2d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera3/output/smoothed_CV_center_bottom_2d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera2/output/smoothed_CV_center_bottom_2d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera3/output/smoothed_CV_center_bottom_2d/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/SceneCaptures_camera2/output/smoothed_CV_center_bottom_2d/csv'];


    # traj_path_truth_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/2019-01-29_13:46:15'];

    # traj_path_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera2/output/smoothed_CV_center_2d_60cm/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera3/output/smoothed_CV_center_2d_60cm/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera2/output/smoothed_CV_center_2d_60cm/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera3/output/smoothed_CV_center_2d_60cm/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/SceneCaptures_camera2/output/smoothed_CV_center_2d_60cm/csv'];


    # traj_path_truth_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/2019-01-29_13:46:15'];

    # traj_path_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera2/output/smoothed_CV_center_2d_80cm/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/SceneCaptures_camera3/output/smoothed_CV_center_2d_80cm/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera2/output/smoothed_CV_center_2d_80cm/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/SceneCaptures_camera3/output/smoothed_CV_center_2d_80cm/csv',\
    #                   '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/SceneCaptures_camera2/output/smoothed_CV_center_2d_80cm/csv'];


    # traj_path_truth_list = ['/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_1/2019-01-28_18:01:00',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_2/2019-01-28_21:05:09',\
    #                         '/media/caor/67c22890-fe0f-4156-9112-04f0cc358d5b/datasets/carla/carla_20190128_3/2019-01-29_13:46:15'];

    traj_path_list = ['/home/caor/Documents/code/datasets/carla_exp_6/camera_1/output/smoothed_CV_center_2d_80cm/csv'];
    traj_path_truth_list = ['/home/caor/Documents/code/datasets/carla_exp_6/2018-11-14_16:41:20'];


    error_xy_list = [];
    error_vxvy_list = [];
    error_psi_rad_list = [];
    nb_traj = 0;

    for traj_path, traj_path_truth in zip(traj_path_list, traj_path_truth_list):
        print('Processing traj_path: {}'.format(traj_path));
        traj_list = util_traj.read_traj_list_from_csv(traj_path);
        traj_truth_list = util_traj.read_traj_list_from_csv(traj_path_truth);

        error_xy, error_vxvy, error_psi_rad, result_traj_list, result_traj_truth_list = compute_precision(traj_list, traj_truth_list, display_hist=False);


        error_xy_list.append(error_xy);
        error_vxvy_list.append(error_vxvy);
        error_psi_rad_list.append(error_psi_rad);

        nb_traj = nb_traj+len(result_traj_list);


    error_xy = np.mean(error_xy_list);
    error_vxvy =  np.mean(error_vxvy_list);
    error_psi_rad = np.mean(error_psi_rad_list);

    print('**************************')
    print('TOTAL Average Error: xy:{} vxvy:{} psi_rad:{}'.format(error_xy, error_vxvy, error_psi_rad));
    print('Trajectories {}'.format(nb_traj))
    # print('Total trajectories time: {}s'.format(float(total_legnth_ms)/float(1e3)))
    print('**************************')




    # ##########################################################
    # # TEMPORARY
    # ##########################################################

    # # Intrinsic camera parameters
    # # fs_read = cv2.FileStorage(config['INPUT_PATH']['CAMERA_CFG_STREET_PATH'], cv2.FILE_STORAGE_READ)
    # fs_read = cv2.FileStorage('/home/caor/Documents/code/trajectory-generator/python/calib/calib_file/carla_area1_sat_cfg.yml', cv2.FILE_STORAGE_READ)
    # cam_matrix_1 = fs_read.getNode('camera_matrix').mat()
    # rot_CF1_F = fs_read.getNode('rot_CF_F').mat()
    # trans_CF1_F = fs_read.getNode('trans_CF_F').mat()
    # dist_coeffs_1 = fs_read.getNode('dist_coeffs').mat()

    # # Construct camera model
    # cam_model_sat = cm.CameraModel(rot_CF1_F, trans_CF1_F, cam_matrix_1, dist_coeffs_1);
    # img_sat_og = cv2.imread('/home/caor/Documents/code/trajectory-generator/python/calib/calib_input/carla_area1_sat.png');

    # show_sat_img(cam_model_sat, img_sat_og, result_traj_list, result_traj_truth_list);


if __name__ == '__main__':

    try:
        main()
    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')