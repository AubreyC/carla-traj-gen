#################################################################################
#
# Trajectory test
#
#################################################################################

import util_traj
import trajectory
import os
import numpy as np
import copy


TRAJ_PATH = 'carla_traj_csv_example/'

def get_traj_in_list(traj_list, id):

    for traj in traj_list:
        if id == traj.get_id():
            return traj;

    return None;

def main():

    # Open img folder
    list_traj_file = os.listdir(TRAJ_PATH);
    list_traj_file.sort(key=lambda f: int(''.join(filter(str.isdigit, f))));

    # List of trajectories:
    traj_list = [];

    for current_index, current_traj_file in enumerate(list_traj_file):

        data_list_csv = util_traj.read_traj_csv(os.path.join(TRAJ_PATH, current_traj_file));

        for data_csv in data_list_csv:

            traj = get_traj_in_list(traj_list, data_csv['id']);

            # If trajectory is not created yet, create it
            if traj is None:
                traj = trajectory.Trajectory(data_csv['id'], data_csv['agent_type']);
                traj_list.append(traj);

            # Add point to the corresponding trajetcory
            traj.add_point(data_csv['timestamp_ms'], \
                           data_csv['x'], \
                           data_csv['y'], \
                           data_csv['vx'], \
                           data_csv['vy'], \
                           data_csv['psi_rad']);


    print('[INFO]: Trajectories {}'.format(len(traj_list)))

    # for traj in traj_list:
    #     print('[INFO]: Traj {} lenght: {}'.format(traj.get_id(), traj.get_length_ms()))

    traj_1 = traj_list[10];
    traj_2 = copy.copy(traj_1);
    traj_3 = traj_list[15];


    error = traj_1.compute_distance_to_traj(traj_2);
    print('traj_1 to traj_2:\n{}'.format(error))


    error = traj_1.compute_distance_to_traj(traj_3);
    print('traj_1 to traj_3:\n{}'.format(error))


    # Test to find the closest match:
    index, traj_min = util_traj.find_closest_traj(traj_1, traj_list);

    print('Closest traj index: {}'.format(index));

if __name__ == '__main__':

    try:
        main()
    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')