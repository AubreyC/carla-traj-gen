# Carla Trajectories Generator

Generate trajectories from carla simulator with:
- Ground truth csv with agent information - Position, Velocity, Orientation, etc
- Camera images from different location - Simulate surveillance caemra

## Start Simulator:

    cd Unreal/CarlaUE4
    ~/Documents/code/carla/UnrealEngine_4.18/Engine/Binaries/Linux/UE4Editor "$PWD/CarlaUE4.uproject"
